# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import logging
from datetime import date, timedelta
from decimal import Decimal
from operator import attrgetter

import polars as pl
from trytond.i18n import gettext
from trytond.model import ModelView, Unique, fields
from trytond.model.exceptions import AccessError
from trytond.modules.account_col.invoice import TYPE_INVOICE_OUT
from trytond.modules.product import round_price
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Bool, Eval, If, Not, Or
from trytond.report import Report
from trytond.transaction import Transaction
from trytond.wizard import (
    Button,
    StateAction,
    StateReport,
    StateTransition,
    StateView,
    Wizard,
)

from .exceptions import (
    PartyMissingAccount,
    SaleMissingSequenceError,
    SaleWriteError,
    ShopUserError,
)
from .query_string import (
    query_agent,
    query_consumer,
    query_party,
    query_sale,
    query_sale_discount,
    query_sale_source,
    schema_query_agent,
    schema_query_consumer,
    schema_query_discount,
    schema_query_party,
    schema_query_sale,
    schema_query_source,
)

logger = logging.getLogger(__name__)
_ZERO = Decimal('0.00')


class Sale(metaclass=PoolMeta):
    __name__ = 'sale.sale'
    shop = fields.Many2One('sale.shop', 'Shop', required=True,
         states={
            'readonly': Or(Bool(Eval('number')), Bool(Eval('lines'))),
        }, depends=['number', 'lines'])
    shop_address = fields.Function(fields.Many2One('party.address',
            'Shop Address'), 'on_change_with_shop_address')
    source = fields.Many2One('sale.source', 'Source')
    payments = fields.One2Many('account.statement.line', 'sale', 'Payments')
    paid_amount = fields.Function(fields.Numeric('Paid Amount', digits=(16, 2)),
        'get_paid_amount')
    residual_amount = fields.Function(fields.Numeric('Residual Amount',
        digits=(16, 2), readonly=True), 'get_residual_amount')

    @classmethod
    def __setup__(cls):
        super(Sale, cls).__setup__()
        cls._buttons.update({
            'wizard_sale_payment': {
                'invisible': Eval('state').in_(['done', 'transferred']),
                'readonly': Not(Bool(Eval('lines'))),
            },
        })
        cls.state_string = super(Sale, cls).state.translated('state')
        t = cls.__table__()
        cls._sql_constraints.extend([
            ('number_uniq', Unique(t, t.shop, t.number),
                'There is another sale with the same number.\n'
                'The number of the sale must be unique!'),
            ])

        shipment_addr_domain = cls.shipment_address.domain[:]
        if shipment_addr_domain:
            cls.shipment_address.domain = [
                'OR',
                shipment_addr_domain,
                [('id', '=', Eval('shop_address', 0))],
                ]
        else:
            cls.shipment_address.domain = [('id', '=', Eval('shop_address'))]
        cls.shipment_address.depends.add('shop_address')
        cls.currency.states['readonly'] = Eval('state') != 'draft'
        cls.currency.depends.add('shop')

    @staticmethod
    def default_company():
        User = Pool().get('res.user')
        user = User(Transaction().user)
        return user.shop.company.id if user.shop else \
            Transaction().context.get('company')

    @fields.depends('company')
    def on_change_company(self):
        User = Pool().get('res.user')
        user = User(Transaction().user)
        if not user.shop:
            super(Sale, self).on_change_company()
        else:
            self.invoice_method = user.shop.sale_invoice_method
            self.shipment_method = user.shop.sale_shipment_method
            self.payment_term = user.shop.payment_term.id

    @staticmethod
    def default_shop():
        User = Pool().get('res.user')
        user = User(Transaction().user)
        return user.shop.id if user.shop else None

    @staticmethod
    def default_description():
        config = Pool().get('sale.configuration')(1)
        if config.default_description:
            return config.default_description

    @staticmethod
    def default_warehouse():
        User = Pool().get('res.user')
        user = User(Transaction().user)
        if user.shop:
            return user.shop.warehouse.id
        else:
            Location = Pool().get('stock.location')
            return Location.search([('type', '=', 'warehouse')], limit=1)[0].id

    @staticmethod
    def default_price_list():
        User = Pool().get('res.user')
        user = User(Transaction().user)
        return user.shop.price_list.id if user.shop and user.shop.price_list else None

    @staticmethod
    def default_shop_address():
        User = Pool().get('res.user')
        user = User(Transaction().user)
        return ((user.shop and user.shop.address and user.shop.address.id) or None)

    @classmethod
    def copy(cls, sales, default=None):
        if default is None:
            default = {}
        default['payments'] = None
        default['source'] = None
        return super(Sale, cls).copy(sales, default)

    @fields.depends('shop', 'party')
    def on_change_shop(self):
        if not self.shop:
            return
        shop = self.shop
        for fname in ('company', 'warehouse', 'currency', 'payment_term'):
            fvalue = getattr(self.shop, fname)
            if fvalue:
                setattr(self, fname, fvalue.id)
        if ((not self.party or not self.party.sale_price_list)
                and shop.price_list):
            self.price_list = shop.price_list.id
        if shop.sale_invoice_method:
            self.invoice_method = shop.sale_invoice_method
        if shop.sale_shipment_method:
            self.shipment_method = shop.sale_shipment_method

        fields = ['pos_authorization', 'computer_authorization', 'electronic_authorization']
        if all(f in shop._fields for f in fields):
            auth_pos, auth_computer, auth_electronic = attrgetter(*fields)(shop)
            if not auth_pos and not auth_computer and auth_electronic:
                self.invoice_type = '1'

    @fields.depends('shop')
    def on_change_with_shop_address(self, name=None):
        return ((self.shop and self.shop.address and
            self.shop.address.id) or None)

    @fields.depends('shop', 'party')
    def on_change_party(self):
        super(Sale, self).on_change_party()
        if self.shop:
            if not self.price_list and self.shop.price_list:
                self.price_list = self.shop.price_list.id
                # self.price_list.rec_name = self.shop.price_list.rec_name
            if not self.payment_term and self.shop.payment_term:
                self.payment_term = self.shop.payment_term.id
                # self.payment_term.rec_name = self.shop.payment_term.rec_name

    @classmethod
    def get_paid_amount(cls, sales, names):
        result = {n: {s.id: Decimal(0) for s in sales} for n in names}
        for name in names:
            for sale in sales:
                for payment in sale.payments:
                    result[name][sale.id] += payment.amount
                result[name][sale.id] += sale.get_total_vouchers_amount()
        return result

    def get_residual_amount(self, name=None):
        total = self.total_amount
        return (total - self.paid_amount)

    @classmethod
    def set_number(cls, sales):
        """
        Fill the number field with the sale shop or sale config sequence
        """
        pool = Pool()
        Config = pool.get('sale.configuration')
        User = Pool().get('res.user')
        config = Config(1)
        user = User(Transaction().user)
        for sale in sales:
            if sale.number:
                continue
            elif sale.shop:
                if sale.total_amount >= Decimal('0'):
                    number = sale.shop.sale_sequence.get()
                else:
                    if not sale.shop.sale_return_sequence:
                        raise SaleMissingSequenceError(
                            gettext('sale_shop.msg_no_return_sequence'))
                    number = sale.shop.sale_return_sequence.get()
            elif user.shop:
                number = user.shop.sale_sequence.get()
            else:
                number = config.sale_sequence.get()
            cls.write([sale], {
                    'number': number,
                    })

    @classmethod
    def create(cls, vlist):
        vlist2 = []
        config = Pool().get('sale.configuration')(1)
        for vals in vlist:
            User = Pool().get('res.user')
            user = User(Transaction().user)
            vals = vals.copy()
            if 'shop' not in vals:
                if not user.shop:
                    raise ShopUserError(
                        gettext('sale_shop.not_sale_shop', s=user.rec_name))
                vals['shop'] = user.shop.id
            vlist2.append(vals)
        sales = super(Sale, cls).create(vlist2)
        if config.on_draft_sequence:
            for sale in sales:
                if sale.invoice_type == '1' or sale.invoice_type == 'C':
                    cls.set_number([sale])
        return sales

    @classmethod
    @ModelView.button_action('sale_shop.wizard_sale_payment')
    def wizard_sale_payment(cls, sales):
        pass

    @classmethod
    def write(cls, *args):
        """
        Only edit Sale users available edit in this shop
        """
        User = Pool().get('res.user')
        user = User(Transaction().user)
        if user.id != 0:
            actions = iter(args)
            for sales, _ in zip(actions, actions, strict=False):
                for sale in sales:
                    if not sale.shop:
                        raise SaleWriteError(
                         gettext('sale_shop.msg_sale_not_shop'))
                    elif sale.shop not in user.shops:
                        raise SaleWriteError(
                            gettext('sale_shop.msg_edit_sale_by_shop'))
        super(Sale, cls).write(*args)

    @classmethod
    def delete(cls, sales):
        for sale in sales:
            if sale.number:
                raise AccessError(
                    gettext('sale.msg_sale_delete_cancel',
                        sale=sale.rec_name))
        super(Sale, cls).delete(sales)

    def create_invoice(self):
        invoice = super(Sale, self).create_invoice()
        if not invoice:
            return
        if invoice.shop and invoice.shop.tax_exempt:
            for line in invoice.lines:
                if not line.product.account_category.account_other_income:
                    raise AccessError(
                    gettext('sale_shop.msg_dont_account_other_income',
                        product=line.product.rec_name))
                line.account = line.product.account_category.account_other_income
        return invoice


class SaleLine(metaclass=PoolMeta):
    __name__ = 'sale.line'
    unit_price_w_tax = fields.Function(fields.Numeric('Unit Price with Tax',
        digits=(16, Eval('_parent_sale', {}).get('currency_digits', 2)),
        depends=['currency'],
        states={
            'invisible': Eval('type') != 'line',
            'readonly': Eval('sale_state') != 'draft',
            }), 'on_change_with_unit_price_w_tax', setter='set_unit_price_w_tax')
    amount_w_tax = fields.Function(fields.Numeric('Amount with Tax',
        digits=(16, Eval('_parent_sale', {}).get('currency_digits', 2)),
        depends=['currency'],
        states={
            'invisible': Eval('type') != 'line',
            'readonly': Eval('sale_state') != 'draft',
            }), 'on_change_with_amount_w_tax', setter='set_amount_w_tax')

    @classmethod
    def __register__(cls, module):
        super().__register__(module)
        table_h = cls.__table_handler__(module)
        # Migration from 7.0.1: drop_column unit_price_full
        if table_h.column_exist('unit_price_full'):
            table_h.drop_column('unit_price_full')

    @staticmethod
    def default_currency_digits():
        Company = Pool().get('company.company')
        if Transaction().context.get('company'):
            company = Company(Transaction().context['company'])
            return company.currency.digits
        return 2

    @staticmethod
    def default_currency():
        Company = Pool().get('company.company')
        if Transaction().context.get('company'):
            company = Company(Transaction().context['company'])
            return company.currency.id

    @fields.depends('unit_price', 'quantity', 'taxes', 'product', 'currency', 'sale', '_parent_sale.sale_date')
    def on_change_with_unit_price_w_tax(self, name=None):
        if not self.currency:
            return 0
        currency_round = self.currency.round
        if not self.quantity or self.unit_price is None or not self.taxes or not self.product:
            return currency_round(Decimal(self.unit_price or 0))
        pool = Pool()
        Tax = pool.get('account.tax')
        date = self.sale.sale_date
        if not date:
            date = pool.get('ir.date').today()
        tax_list = Tax.compute(self.taxes, Decimal(self.unit_price), 1, date)
        tax_amount = sum([t['amount'] for t in tax_list], _ZERO)
        classification_tax = any(t.classification_tax == '02' for t in self.taxes)
        extra_tax = 0
        if self.product and hasattr(self.product, 'extra_tax') and self.product.extra_tax and classification_tax:
            extra_tax = self.product.extra_tax
        return currency_round(tax_amount + extra_tax + Decimal(self.unit_price))

    @fields.depends(
        'quantity', 'unit_price_w_tax', 'currency', 'taxes', 'sale', '_parent_sale.sale_date',
        methods=['on_change_with_discount_rate', 'on_change_with_discount_amount', 'on_change_with_discount'])
    def on_change_unit_price_w_tax(self):
        if not self.unit_price_w_tax or not self.quantity:
            return 0
        currency_round = self.currency.round
        if self.taxes:
            pool = Pool()
            Tax = pool.get('account.tax')
            res = Tax.reverse_compute(self.unit_price_w_tax, self.taxes, self.sale.sale_date)
            self.unit_price = round_price(res)
            self.amount_w_tax = currency_round(self.unit_price_w_tax * Decimal(self.quantity))
        else:
            self.unit_price = self.unit_price_w_tax
            self.amount_w_tax = currency_round(self.unit_price_w_tax * Decimal(self.quantity))
        self.discount_rate = self.on_change_with_discount_rate()
        self.discount_amount = self.on_change_with_discount_amount()
        self.discount = self.on_change_with_discount()

    @classmethod
    def set_unit_price_w_tax(cls, lines, name, value):
        pass

    @fields.depends('unit_price_w_tax', 'quantity', 'product', 'currency')
    def on_change_with_amount_w_tax(self, name=None):
        if self.unit_price_w_tax is None or not self.quantity or not self.product or not self.currency:
            return
        currency_round = self.currency.round
        return currency_round(self.unit_price_w_tax * Decimal(self.quantity))

    @fields.depends('amount_w_tax', 'quantity', 'taxes', 'currency',
        methods=['on_change_with_discount_rate', 'on_change_with_discount_amount', 'on_change_with_discount'])
    def on_change_amount_w_tax(self):
        pool = Pool()
        Tax = pool.get('account.tax')
        currency_round = self.currency.round
        if self.taxes and self.amount_w_tax is not None and self.quantity:
            self.unit_price_w_tax = currency_round(self.amount_w_tax / Decimal(self.quantity))
            res = Tax.reverse_compute(self.unit_price_w_tax, self.taxes)
            self.unit_price = round_price(res)
        self.discount_rate = self.on_change_with_discount_rate()
        self.discount_amount = self.on_change_with_discount_amount()
        self.discount = self.on_change_with_discount()

    @classmethod
    def set_amount_w_tax(cls, lines, name, value):
        pass

    @fields.depends(
        'product', 'unit', 'quantity', 'description',
        '_parent_sale.party', '_parent_sale.currency', '_parent_sale.sale_date')
    def on_change_product(self):
        super(SaleLine, self).on_change_product()
        if self.product:
            desc = self.product.template.name
            if self.product.description:
                desc += ' | ' + self.product.description
            self.description = desc

    # @fields.depends('product', 'quantity', 'unit', 'taxes',
    #     '_parent_sale.currency', '_parent_sale.party',
    #     '_parent_sale.sale_date', 'unit_price_full')
    # def on_change_quantity(self):
    #     super(SaleLine, self).on_change_quantity()
    #     if self.unit_price_full:
    #         self.on_change_unit_price_full()


class SaleDetailedStart(ModelView):
    "Sale Detailed Start"
    __name__ = 'sale_shop.sale_detailed.start'
    company = fields.Many2One('company.company', 'Company', required=True)
    party = fields.Many2One('party.party', 'Party')
    start_date = fields.Date('Start Date', required=True)
    end_date = fields.Date('End Date', required=True)
    from_invoice = fields.Boolean('From Invoice', states={'invisible': Eval('only_courtesy', False)})
    only_courtesy = fields.Boolean('Only Courtesy')
    shops = fields.Many2Many('sale.shop', None, None, 'Shops')
    agents = fields.Many2Many('commission.agent', None, None, 'Agents')

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_end_date():
        Date_ = Pool().get('ir.date')
        return Date_.today()

    @staticmethod
    def default_start_date():
        Date_ = Pool().get('ir.date')
        today = Date_.today()
        return date(today.year, today.month, 1)

    @classmethod
    def view_attributes(cls):
        view_attrs = super().view_attributes()
        Sale = Pool().get('sale.sale')
        if 'courtesy' not in Sale._fields:
            return view_attrs

        return [*view_attrs,
            ('/form/field[@name="only_courtesy"]',
            'states', {
                'invisible': True,
                }),
            ]

    @fields.depends('only_courtesy', 'from_invoice')
    def on_change_only_courtesy(self, name=None):
        if self.only_courtesy:
            self.from_invoice = False


class SaleDetailed(Wizard):
    "Sale Detailed"
    __name__ = 'sale_shop.sale_detailed'
    start = StateView(
        'sale_shop.sale_detailed.start',
        'sale_shop.sale_detailed_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-print', default=True),
        ])
    print_ = StateReport('sale_shop.sale_detailed_report')

    def do_print_(self, action):
        shops = [s.id for s in self.start.shops]
        agents = [s.id for s in self.start.agents]
        data = {
            'ids': [],
            'company': self.start.company.id,
            'start_date': self.start.start_date,
            'end_date': self.start.end_date,
            'from_invoice': self.start.from_invoice,
            'only_courtesy': self.start.only_courtesy,
            'shops': shops,
            'agents': agents,
            'party': self.start.party.id if self.start.party else None,
        }
        return action, data

    def transition_print_(self):
        return 'end'


class SaleDetailedReport(Report):
    "Sale Shop Sale Detailed Report"
    __name__ = 'sale_shop.sale_detailed_report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        pool = Pool()
        Company = pool.get('company.company')
        modules = ['sale_pos_frontend']
        modules_active = pool.get('ir.module').search([
            ('name', 'in', modules),
            ('state', '=', 'activated')], limit=1)
        data['modules_active'] = {m.name for m in modules_active}
        states_string = {
            'validated': 'Validado',
            'posted': 'Contabilizado',
            'paid': 'Pagado',
            'processing': 'Procesado',
            'done': 'Finalizado',
            'transferred': 'Transferido',
        }
        invoice_type_string = {}
        for k, s in TYPE_INVOICE_OUT:
            invoice_type_string[k] = s

        kind_string = {
            'take_away': 'PARA LLEVAR',
            'delivery': 'DOMICILIO',
            'to_table': 'A LA MESA',
            'catering': 'CATERING',
            'room_service': 'SERVICIO A LA HABITACION',
        }

        df = cls.get_data_query(data)
        df_grouped = cls.get_grouped_df(df)
        report_context['company'] = Company(data['company'])
        report_context['state'] = states_string
        report_context['kind'] = kind_string
        report_context['invoice_type'] = invoice_type_string
        # try:
        if not df.is_empty():
            records = df.to_dicts()
            report_context['records'] = records
            sales_by_agent = cls.get_sales_by_agent(df_grouped).to_dicts()
            report_context['sales_by_agent'] = sales_by_agent
            report_context['sales_by_shop'] = cls.get_sales_by_shop(df_grouped).to_dicts()
            report_context['sales_daily'] = cls.get_sales_daily(df, df_grouped).to_dicts()
            report_context['sales_by_kind'] = cls.get_sales_by_kind(df).to_dicts()
            report_context['sales_by_category_product'] = cls.get_sales_by_category_product(df).to_dicts()
            report_context['cost_of_sales_per_product'] = cls.get_cost_of_sales_per_product(df).to_dicts()
        else:
            report_context['records'] = []
            report_context['sales_by_agent'] = []
            report_context['sales_by_shop'] = []
            report_context['sales_daily'] = []
            report_context['sales_by_kind'] = []
            report_context['sales_by_category_product'] = []
            report_context['cost_of_sales_per_product'] = []
        return report_context

    @classmethod
    def get_schema_query_sale(cls, data):
        schema = schema_query_sale
        if not data.get('from_invoice'):
            value = {
                'consumer_name': pl.String,
                'consumer_phone': pl.String,
                'source': pl.Int64,
                'auth_discount': pl.Int64,
            }
            schema.update(value)
        return schema

    @classmethod
    def get_grouped_df(cls, df):
        df_select = None
        if not df.is_empty():
            df_select = df.select(['shop_name', 'agent_name', 'date', 'payment_type', 'untaxed_amount']).sort(['payment_type', 'untaxed_amount'])
            df_select = df.with_columns(
                [
                    pl.col('kind').fill_null('others'),
                    pl.when(
                    pl.col('untaxed_amount') >= 0, pl.col('payment_type') == '1').then(1)
                    .when(pl.col('untaxed_amount') >= 0, pl.col('payment_type') == '2').then(2)
                    .when(pl.col('untaxed_amount') < 0, pl.col('payment_type') == '2').then(3)
                    .otherwise(4).alias('type_doc')])

            df_select = df_select.group_by(
                    ['shop_name', 'agent_name', 'date', 'type_doc'],
                ).agg(pl.col('untaxed_amount').sum())
        return df_select

    @classmethod
    def get_sales_by_agent(cls, df):
        df = df.with_columns(pl.col('agent_name').cast(pl.String))
        df = df.select(
                ['shop_name', 'agent_name', 'type_doc', 'untaxed_amount'],
            ).pivot(
                index=["shop_name", 'agent_name'],
                columns='type_doc',
                values='untaxed_amount',
                aggregate_function="sum")
        return df

    @classmethod
    def get_sales_by_shop(cls, df):
        df = df.select(
                ['shop_name', 'type_doc', 'untaxed_amount'],
            ).pivot(
                index=["shop_name"],
                columns='type_doc',
                values='untaxed_amount',
                aggregate_function="sum")
        return df

    @classmethod
    def get_sales_by_category_product(cls, df):
        df_category = df.select(
                ['shop_name', 'category_admin', 'product_name', 'quantity', 'untaxed_amount', 'total_price_taxed'],
            ).group_by(['shop_name', 'category_admin', 'product_name'],
            ).agg([
                pl.col('quantity').sum(),
                pl.col('untaxed_amount').sum(),
                pl.col('total_price_taxed').sum()],
            ).sort('shop_name', 'category_admin', 'product_name')
        return df_category

    @classmethod
    def get_sales_by_kind(cls, df):
        df = df.select(['shop_name', 'number', 'date', 'kind', 'untaxed_amount'],
            ).group_by(['shop_name', 'number', 'date', 'kind']).agg(pl.col('untaxed_amount').sum())
        df_sum = df.select(
                ['shop_name', 'kind', 'untaxed_amount'],
            ).pivot(
                index=["shop_name"],
                columns='kind',
                values=['untaxed_amount'],
                aggregate_function="sum")
        df_sum = df_sum.with_columns(df_sum.select(pl.exclude(['shop_name']),
                    ).sum_horizontal().alias('total_untaxed_amount'))

        df_count = df.select(['shop_name', 'kind']).group_by(['shop_name', 'kind'],
            ).agg(pl.count('kind').alias('count'),
            ).pivot(
                index=['shop_name'],
                columns='kind',
                values='count',
                aggregate_function='sum')
        df_count = df_count.with_columns(df_count.select(pl.exclude(['shop_name'])).sum_horizontal().alias('total_count'))

        result = df_sum.join(df_count, on='shop_name', how='left', suffix='_count')
        return result

    @classmethod
    def get_sales_daily(cls, df, df_grouped):
        df_amount_taxed = df.select(['date', 'taxes', 'total_price_taxed', 'untaxed_amount'],
            ).group_by(['date'],
            ).agg([
                pl.col('untaxed_amount').sum(),
                pl.col('taxes').sum(),
                pl.col('total_price_taxed').sum(),
            ])

        df_num_records = df.select(['date', 'record_sale'],
            ).unique(subset=['date', 'record_sale'],
            ).group_by(['date']).agg([pl.col('record_sale').count()])

        df_amounts = df_grouped.select(
                ['date', 'type_doc', 'untaxed_amount'],
            ).pivot(
                index=["date"],
                columns='type_doc',
                values='untaxed_amount',
                aggregate_function="sum",
            ).sort('date')

        df_amounts = df_amounts.join(df_num_records, on='date', how='left',
            ).join(df_amount_taxed, on='date', how='left')

        return df_amounts

    @classmethod
    def get_cost_of_sales_per_product(cls, df):
        df = df.select(
                [
                    'product_code', 'product_name', 'category_admin', 'uom',
                    'quantity', 'utility', 'untaxed_amount', 'total_cost_price_sl'],
            ).group_by(['category_admin', 'product_code', 'product_name', 'uom'],
            ).agg(
                [
                    pl.col('quantity').sum(),
                    pl.col('utility').sum(),
                    pl.col('total_cost_price_sl').mean().alias('cost_price_mean'),
                    pl.col('total_cost_price_sl').sum().alias('total_cost_price'),
                    pl.col('untaxed_amount').sum().alias('total_untaxed_amount'),
                ])

        df = df.with_columns(
            ((df['total_untaxed_amount'] / df['total_cost_price']) - 1).alias('margin'),
        )

        return df

    @classmethod
    def get_data_query(cls, data):
        cursor = Transaction().connection.cursor()
        where = cls.get_where(data)
        table = 'sale_sale'
        column_table = 'sale'
        table_line = 'sale_line'
        kind = 'kind'
        states = cls.get_states(data)
        other_columns = ''
        df_consumer = None
        df_source = None
        df_discount = None

        if data.get('from_invoice') and not data.get('only_courtesy'):
            table = 'account_invoice'
            column_table = 'invoice'
            table_line = 'account_invoice_line'
            other_columns += ' ,i.sale_kind AS kind ' if 'sale_pos_frontend' in data['modules_active'] else ''
        elif 'sale_pos_frontend' in data['modules_active']:
            other_columns += f', i.source, i.invoice_number , i.{kind} AS kind, l.auth_discount AS auth_discount'
            df_source = pl.read_database(query_sale_source, cursor, schema_overrides=schema_query_source)
            df_discount = pl.read_database(query_sale_discount, cursor, schema_overrides=schema_query_discount)
            query_consumer_ = query_consumer.format(
                start_date=data['start_date'],
                end_date=data['end_date'],
                where=where,
                states=states,
                column_table=column_table,
            )
            df_consumer = pl.read_database(query_consumer_, cursor, schema_overrides=schema_query_consumer)
        query_sale_ = query_sale.format(
            start_date=data['start_date'],
            end_date=data['end_date'],
            column_table=column_table,
            other_columns=other_columns,
            table=table,
            table_line=table_line,
            states=states,
            kind=kind,
            where=where,
        )

        df_agent = pl.read_database(
            query_agent,
            cursor,
            schema_overrides=schema_query_agent)

        query_party_ = query_party.format(
            start_date=data['start_date'],
            end_date=data['end_date'],
            table=table,
            column_table=column_table,
            where=where,
            states=states,
        )

        df_party = pl.read_database(
            query_party_,
            cursor,
            schema_overrides=schema_query_party)

        schema = cls.get_schema_query_sale(data)
        df = pl.read_database(query_sale_, cursor, schema_overrides=schema)
        if not df.is_empty():

            df = df.join(df_agent, on='agent', how="left")
            df = df.join(df_party, on='record_sale', how="left")
            if df_consumer is not None:
                df = df.join(df_consumer, on='record_sale', how="left")

            if df_source is not None and not df_source.is_empty():
                df = df.join(df_source, on='source', how='left')
            if df_discount is not None and not df_discount.is_empty():
                non_null_count = df.filter(pl.col("auth_discount").is_not_null()).height
                if non_null_count > 0:
                    df = df.join(df_discount, on="auth_discount", how="left")
                else:
                    df = df.with_columns(pl.lit("").alias("auth_discount_name"))
            if 'kind' not in df.columns:
                df = df.with_columns(pl.lit('others').alias('kind'))

            tax_rate_unit_price = df['tax_rate'] * df['unit_price']
            utility = (df['unit_price'] - df['cost_price_sl']) * df['quantity']
            total_cost_price_sl = df['cost_price_sl'] * df['quantity']
            df = df.with_columns([
                (df['unit_price'] * df['quantity']).alias('untaxed_amount'),
                (total_cost_price_sl).alias('total_cost_price_sl'),
                ((df['tax_rate'] * df['cost_price_sl']) + df['cost_price_sl'] + df['tax_amount']).alias('cost_price_taxed'),
                ((tax_rate_unit_price + df['tax_amount']) * df['quantity']).alias('taxes'),
                (tax_rate_unit_price + df['unit_price'] + df['tax_amount']).alias('unit_price_taxed'),
                ((tax_rate_unit_price + df['unit_price'] + df['tax_amount']) * df['quantity']).alias('total_price_taxed'),
                (100 - (100 * df['unit_price'] / df['base_price'])).alias('discount'),
                ((df['base_price'] - df['unit_price']) * df['quantity']).alias('total_discount'),
                (utility).alias('utility'),
                (utility / total_cost_price_sl).alias('utility_rate'),
            ])
        return df

    @classmethod
    def get_states(cls, data):
        states = "('transferred', 'processing', 'done')"
        if data.get('from_invoice') and not data.get('only_courtesy'):
            states = "('posted', 'paid', 'validate')"
        return states

    @classmethod
    def get_where(cls, data):
        where = ''
        shops = data.get('shops')
        if data.get('from_invoice'):
            where += " AND i.type='out'"

        if len(shops) == 1:
            where += f' AND i.shop={shops[0]} '
        elif shops:
            where += f' AND i.shop IN {tuple(shops)!s} '

        agents = data.get('agents')
        if len(agents) == 1:
            where += f' AND i.agent={agents[0]} '
        elif agents:
            where += f' AND i.agent IN {tuple(agents)!s} '

        if data.get('only_courtesy'):
            where += " AND i.courtesy='t' "

        if data.get('party'):
            where += f" AND i.party={data.get('party')} "
        return where

    # @classmethod
    # def get_join(cls, data):
    #     return ''


class SaleBySupplierStart(ModelView):
    "Sale By Supplier Start"
    __name__ = 'sale_shop.sale_by_supplier.start'
    start_date = fields.Date('Start Date', required=True)
    end_date = fields.Date('End Date', required=True)
    suppliers = fields.Many2Many('party.party', None, None, 'Suppliers')
    shop = fields.Many2One('sale.shop', 'Shop')
    company = fields.Many2One('company.company', 'Company', required=True)
    detailed = fields.Boolean('Detailed')

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_end_date():
        Date_ = Pool().get('ir.date')
        return Date_.today()


class PrintSaleBySupplier(Wizard):
    "Sale By Supplier"
    __name__ = 'sale_shop.print_sale_by_supplier'
    start = StateView(
        'sale_shop.sale_by_supplier.start',
        'sale_shop.print_sale_by_supplier_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-print', default=True),
        ])
    print_ = StateReport('sale_shop.sale_by_supplier_report')

    def do_print_(self, action):
        suppliers = [s.id for s in self.start.suppliers]
        shop_id = None
        shop_name = ''
        if self.start.shop:
            shop_id = self.start.shop.id
            shop_name = self.start.shop.name
        data = {
            'ids': [],
            'company': self.start.company.id,
            'start_date': self.start.start_date,
            'end_date': self.start.end_date,
            'suppliers': suppliers,
            'detailed': self.start.detailed,
            'shop': shop_id,
            'shop_name': shop_name,
        }
        return action, data

    def transition_print_(self):
        return 'end'


class SaleBySupplier(Report):
    "Sale By Supplier Report"
    __name__ = 'sale_shop.sale_by_supplier_report'

    @classmethod
    def get_last_purchase(cls, product_id):
        PurchaseLine = Pool().get('purchase.line')
        lines = PurchaseLine.search([
            ('product', '=', product_id),
            ('purchase.state', 'in', ('processing', 'done')),
        ], order=[('create_date', 'DESC')], limit=1)
        if lines:
            line = lines[0]
            return (line.purchase.purchase_date, line.quantity)
        else:
            return None, None

    @classmethod
    def get_margin(cls, pline):
        res = 0
        total_cost = pline['cost_price'] * pline['quantity']
        if pline['amount'] != 0:
            res = 1 - (total_cost / pline['amount'])
        return res

    @classmethod
    def get_last_sales(cls, product_id, end_date, days):
        InvoiceLine = Pool().get('account.invoice.line')
        start_date = end_date - timedelta(days=days)
        lines = InvoiceLine.search([
            ('product', '=', product_id),
            ('invoice.invoice_date', '>=', start_date),
            ('invoice.invoice_date', '<=', end_date),
            ('invoice.state', 'in', ('posted', 'paid')),
            ('invoice.type', '=', 'out'),
            ('type', '=', 'line'),
        ])
        return sum([l.quantity for l in lines]), start_date

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        pool = Pool()
        Company = pool.get('company.company')
        Party = pool.get('party.party')
        ProductSupplier = pool.get('purchase.product_supplier')
        company = Company(data['company'])
        cursor = Transaction().connection.cursor()
        dom_suppliers = [
            ('template', '!=', None),
        ]
        total_sales = []
        total_sales_append = total_sales.append
        parties_query = ''
        start_date = data['start_date']
        end_date = data['end_date']
        delta = (data['end_date'] - data['start_date']).days + 1

        if data['suppliers']:
            dom_suppliers.append([
                ('party', 'in', data['suppliers']),
            ])
            if len(data['suppliers']) == 1:
                supplier = data['suppliers'][0]
                parties_query = f' AND pu.party={supplier}'
            else:
                parties = str(tuple(data['suppliers']))
                parties_query = f' AND pu.party IN {parties}'

        products_suppliers = ProductSupplier.search(dom_suppliers)
        products_dict = {}
        for ps in products_suppliers:
            if ps.template.products:
                product_id = ps.template.products[0].id
                products_dict[product_id] = ps.party.id

        products_query = ''
        if data['suppliers']:
            keys = products_dict.keys()
            if len(keys) == 1:
                product_id = keys[0]
                products_query = f' AND al.product={product_id} '
            elif keys:
                products_ids = str(tuple(keys))
                products_query = f' AND al.product IN {products_ids} '

        shops_query = ''
        start_date = data['start_date']
        if data['shop']:
            shop = data['shop']
            shops_query = f' AND ai.shop={shop} '
        query = f"""
            SELECT
                    al.product,
                    pp.code,
                    pt.name,
                    SUM(al.unit_price * al.quantity) AS amount,
                    SUM(pc.cost_price * al.quantity) AS cost,
                    SUM(al.quantity) AS quantity
                FROM account_invoice_line AS al
                JOIN account_invoice AS ai ON al.invoice=ai.id
                JOIN product_product AS pp ON al.product=pp.id
                JOIN product_template AS pt ON pp.template=pt.id
                JOIN product_cost_price AS pc ON pc.product=pp.id
                WHERE ai.invoice_date>='{start_date}' AND ai.invoice_date<='{end_date}'
                    AND ai.state IN ('posted', 'paid')
                    AND ai.type='out' AND al.type='line'
                    {products_query}
                    {shops_query}
                GROUP BY al.product, pp.code, pt.name
        """
        cursor.execute(query)
        _records = cursor.fetchall()

        def compute_margin(sales, costs):
            margin = 0
            if sales and sales != 0:
                margin = 1 - costs / sales
            return margin

        suppliers = {}

        def _rec2dict(rec):
            return {
                'code': rec[1],
                'name': rec[2],
                'amount': rec[3],
                'quantity': rec[5],
                'margin': compute_margin(rec[3], rec[4]),
                'daily_sale': rec[5] / delta,
            }

        for rec in _records:
            total_sales_append(rec[3])
            pd_id = rec[0]
            party_id = products_dict.get(pd_id, 0)
            try:
                if data['detailed']:
                    suppliers[party_id]['products'].append(_rec2dict(rec))
                suppliers[party_id]['sales'].append(rec[3])
                suppliers[party_id]['costs'].append(rec[4])
            except Exception:
                parties = Party.search_read([
                    ('id', '=', int(party_id)),
                ], fields_names=['name'])
                if not parties:
                    continue
                party = parties[0]
                party_id = party['id']
                suppliers[party_id] = {
                    'name': party['name'],
                    'sales': [rec[3]],
                    'costs': [rec[4]],
                }
                if data['detailed']:
                    suppliers[party_id]['products'] = [_rec2dict(rec)]

        for key, value in suppliers.items():
            suppliers[key]['sales'] = sum(value['sales'])
            suppliers[key]['costs'] = sum(value['costs'])

        # Maybe we need this in the future
        #         sales_30, date_ = cls.get_last_sales(pline['product_id'],
        #             data['start_date'], 30)
        #         sales_60, _ = cls.get_last_sales(pline['product_id'],
        #             date_, 60)

        records = suppliers.values()
        report_context['records'] = records
        report_context['start_date'] = data['start_date']
        report_context['end_date'] = data['end_date']
        report_context['today'] = date.today()
        report_context['company'] = company.party.name
        report_context['shop'] = data['shop_name']
        report_context['total_sales'] = sum(total_sales)
        report_context['detailed'] = data['detailed']
        report_context['compute_margin'] = compute_margin
        return report_context


# class SaleShopDetailedStart(ModelView):
#     "Sale Shop Detailed Start"
#     __name__ = 'sale_shop.sale_detailed.start'
#     start_date = fields.Date('Start Date', required=True)
#     end_date = fields.Date('End Date', required=True)
#     company = fields.Many2One('company.company', 'Company', required=True)
#     agent = fields.Many2One('commission.agent', 'Agent')
#     party = fields.Many2One('party.party', 'Party')
#     product = fields.Many2One('product.product', 'Product')
#     shop = fields.Many2One('sale.shop', 'Shop')
#     categories = fields.Many2Many('product.category', None, None, 'Categories')
#     only_courtesy = fields.Boolean('Only Courtesy', states={'invisible': False})

#     @staticmethod
#     def default_company():
#         return Transaction().context.get('company')

#     @classmethod
#     def view_attributes(cls):
#         view_attrs = super().view_attributes()
#         Sale = Pool().get('sale.sale')
#         if 'courtesy' not in Sale._fields:
#             return view_attrs
#         return [*view_attrs,
#             ('/form/field[@name="only_courtesy"]',
#             'states', {
#                 'invisible': True,
#                 }),
#             ]


# class SaleShopDetailed(Wizard):
#     "Sale Shop Detailed"
#     __name__ = 'sale_shop.sale_detailed'
#     start = StateView(
#         'sale_shop.sale_detailed.start',
#         'sale_shop.sale_shop_detailed_start_view_form', [
#             Button('Cancel', 'end', 'tryton-cancel'),
#             Button('Print', 'print_', 'tryton-ok', default=True),
#         ])
#     print_ = StateAction('sale_shop.report_sale_detailed')

#     def do_print_(self, action):
#         category_ids = []
#         agent_id = None
#         party_id = None
#         product_id = None
#         shop_id = None
#         if self.start.agent:
#             agent_id = self.start.agent.id
#         if self.start.shop:
#             shop_id = self.start.shop.id
#         if self.start.party:
#             party_id = self.start.party.id
#         if self.start.product:
#             product_id = self.start.product.id
#         if self.start.categories:
#             category_ids = [acc.id for acc in self.start.categories]
#         # if self.start.categories:
#         #     data['categories'] = [c.id for c in self.start.categories]
#         data = {
#             'company': self.start.company.id,
#             'start_date': self.start.start_date,
#             'end_date': self.start.end_date,
#             'agent': agent_id,
#             'party': party_id,
#             'product': product_id,
#             'categories': category_ids,
#             'shop': shop_id,
#             'only_courtesy': self.start.only_courtesy,
#         }
#         return action, data

#     def transition_print_(self):
#         return 'end'


# class SaleShopDetailedReport(Report):
#     "Sale Shop Detailed Report"
#     __name__ = 'sale_shop.report_sale_detailed'

#     @classmethod
#     def get_context(cls, records, header, data):
#         report_context = super().get_context(records, header, data)
#         pool = Pool()
#         Company = pool.get('company.company')
#         Category = pool.get('product.template-product.category')

#         categories = Category.search([('category', 'in', data['categories'])])
#         products_ids = [c.template.id for c in categories]
#         if not data['only_courtesy']:
#             InvoiceLine = pool.get('account.invoice.line')
#             invoice_line_dom = [
#                 ('invoice.invoice_date', '>=', data['start_date']),
#                 ('invoice.invoice_date', '<=', data['end_date']),
#                 ('invoice.company', '=', data['company']),
#                 ('invoice.type', '=', 'out'),
#                 ('invoice.state', 'in', ['posted', 'paid', 'validated']),
#             ]
#             if data['agent']:
#                 invoice_line_dom.append(
#                     ('invoice.agent', '=', data['agent']),
#                 )
#             if data['shop']:
#                 invoice_line_dom.append(
#                     ('invoice.shop', '=', data['shop']),
#                 )
#             if data['party']:
#                 invoice_line_dom.append(
#                     ('invoice.party', '=', data['party']),
#                 )
#             if data['product']:
#                 invoice_line_dom.append(
#                     ('product', '=', data['product']),
#                 )
#             if data['categories']:
#                 if products_ids:
#                     invoice_line_dom.append(
#                         ('product.template', 'in', products_ids),
#                     )
#                 else:
#                     invoice_line_dom.append(
#                         ('product.template.account_category', 'in', data['categories']),
#                     )
#             config = pool.get('sale.configuration')(1)
#             products_exception = []
#             if hasattr(config, 'tip_product') and config.tip_product and config.exclude_tip_and_delivery:
#                 products_exception.append(config.tip_product.id)
#             if hasattr(config, 'delivery_product') and config.delivery_product and config.exclude_tip_and_delivery:
#                 products_exception.append(config.delivery_product.id)

#             if len(products_exception) > 0:
#                 invoice_line_dom.append(('product', 'not in', products_exception))

#             start_lines = InvoiceLine.search(
#                 invoice_line_dom, order=[('invoice.invoice_date', 'ASC')],
#             )
#         elif data['only_courtesy']:
#             SaleLine = pool.get('sale.line')
#             sale_line_dom = [
#                 ('sale.sale_date', '>=', data['start_date']),
#                 ('sale.sale_date', '<=', data['end_date']),
#                 ('sale.company', '=', data['company']),
#                 ('sale.state', '=', 'done'),
#                 ('sale.courtesy', '=', True),
#             ]
#             if data['agent']:
#                 sale_line_dom.append(('sale.agent', '=', data['agent']))
#             if data['shop']:
#                 sale_line_dom.append(('sale.shop', '=', data['shop']))
#             if data['party']:
#                 sale_line_dom.append(('sale.party', '=', data['party']))
#             if data['product']:
#                 sale_line_dom.append(('product', '=', data['product']))
#             if data['categories']:
#                 if products_ids:
#                     sale_line_dom.append(('product.template', 'in', products_ids))
#                 else:
#                     sale_line_dom.append(
#                         ('product.template.account_category', 'in', data['categories']),
#                     )

#             start_lines = SaleLine.search(
#                 sale_line_dom, order=[('sale.sale_date', 'ASC')],
#             )
#         lines = []
#         amount = []
#         # amount_w_tax = []
#         for line in start_lines:
#             if line.type == 'line' and not line.product:
#                 raise SaleValidationError(
#                     gettext('sale_shop.msg_sale_not_product', s=line.sale.number,
#                 ))

#             amount.append(line.amount)
#             lines.append(line)
#         report_context['amount'] = sum(amount)
#         report_context['records'] = lines
#         report_context['company'] = Company(data['company'])
#         return report_context


class SaleByMonthStart(ModelView):
    "Sale By Month Start"
    __name__ = 'sale_shop.sale_by_month.start'

    fiscalyear = fields.Many2One('account.fiscalyear', 'Fiscal Year',
        required=True,
        domain=[
            ('company', '=', Eval('company')),
            ],
        depends=['company'])
    start_date = fields.Date('Start Date', required=True, domain=[
        If(Eval('end_date') & Eval('start_date'),
            ('start_date', '<=', Eval('end_date')),
            ()),
    ])
    end_date = fields.Date('End Date', required=True, domain=[
        If(Eval('start_date') & Eval('end_date'),
            ('end_date', '>=', Eval('start_date')),
            ()),
    ])
    company = fields.Many2One('company.company', 'Company', required=True)
    shops = fields.Many2Many('sale.shop', None, None, 'Shops',
        depends=['company'],
        domain=[
                ('company', '=', Eval('company')),
        ])
    agents = fields.Many2Many(
        'commission.agent', None, None, 'Agents')
    payment_methods = fields.Many2Many(
        'account.invoice.payment_term', None, None, 'Payment Method')
    group_by = fields.Selection([
            ('agent', 'Agent'),
            ('shop', 'Shop'),
            ('kind', 'Kind'),
            ('payment_term', 'Payment Term'),
            ('product', 'Product'),
        ], 'Group By')
    group_by_string = group_by.translated('group_by')
    from_invoice = fields.Boolean('From Invoice')

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_group_by():
        return 'shop'

    @staticmethod
    def default_fiscalyear():
        FiscalYear = Pool().get('account.fiscalyear')
        return FiscalYear.find(
            Transaction().context.get('company'), test_state=False).id

    @classmethod
    def get_selection_group_by(cls):
        return [
            ('agent', 'Agent'),
            ('shop', 'Shop'),
            ('kind', 'Kind'),
            ('payment_term', 'Payment Term'),
            ('product', 'Product'),
        ]

    @fields.depends('fiscalyear', 'start_date', 'end_date')
    def on_change_fiscalyear(self):
        if self.fiscalyear:
            self.start_date = self.fiscalyear.start_date
            self.end_date = self.fiscalyear.end_date
        else:
            self.start_date = None
            self.end_date = None


class SaleByMonth(Wizard):
    "Sale By Month"
    __name__ = 'sale_shop.sale_by_month.wizard'
    start = StateView(
        'sale_shop.sale_by_month.start',
        'sale_shop.sale_by_month_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-ok', default=True),
        ])
    print_ = StateAction('sale_shop.sale_by_month_report')

    def do_print_(self, action):
        data = {
            'ids': [],
            'company': self.start.company.id,
            'company_name': self.start.company.rec_name,
            'start_date': self.start.start_date,
            'end_date': self.start.end_date,
            'shops': None,
            'payment_terms': None,
            'agents': None,
            'group_by': self.start.group_by,
            'group_by_name': self.start.group_by_string,
            'from_invoice': self.start.from_invoice,
        }
        shops = self.start.shops
        if shops:
            data['shops'] = [s.id for s in self.start.shops]
        if self.start.payment_methods:
            data['payment_terms'] = [s.id for s in self.start.payment_methods]
        if self.start.agents:
            data['agents'] = [s.id for s in self.start.agents]
        return action, data

    def transition_print_(self):
        return 'end'


class SaleByMonthReport(Report):
    __name__ = 'sale_shop.sale_by_month_report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        cursor = Transaction().connection.cursor()
        Company = Pool().get('company.company')

        data_group, total_months = cls.get_data_sale_group_by(data, cursor)
        report_context['records'] = data_group.values()
        report_context['total'] = total_months
        report_context['company'] = Company(data['company'])
        report_context['company_name'] = Company(data['company']).rec_name
        return report_context

    @classmethod
    def get_data_sale_group_by(cls, data, cursor):

        state = "and a.state in ('processing', 'done')"
        table = 'sale_sale'
        column_date = 'a.sale_date'
        column_amount = 'SUM(untaxed_amount_cache) AS total_untaxed'

        if data['from_invoice']:
            state = " and a.state in ('validated', 'posted', 'paid') and a.type='out'"
            table = 'account_invoice'
            column_date = 'a.invoice_date'

        group_by = data['group_by']
        join = ''
        tables_join = {
            'payment_term': 'account_invoice_payment_term',
            'agent': 'party_party',
            'shop': 'sale_shop',
            'product': 'product_template',
        }

        if group_by == 'kind':
            if data['from_invoice']:
                group_by = 'sale_kind'

        elif group_by == 'product':
            table_join = tables_join[data['group_by']]
            line = 'account_invoice_line' if data['from_invoice'] else 'sale_line'
            line_join = 'invoice' if data['from_invoice'] else 'sale'

            join = f"""LEFT JOIN {line} as c on a.id = c.{line_join}
                LEFT JOIN product_product as e on e.id = c.product
                LEFT JOIN product_template as b on b.id = e.template
            """
            column_amount = 'SUM(c.unit_price*c.quantity) AS total_untaxed'
            group_by = 'b.name'
        elif group_by == 'agent':
            join = """
                LEFT JOIN commission_agent as e on e.id = a.agent
                LEFT JOIN party_party as b on b.id = e.party
            """
            group_by = 'b.name'
        else:
            table_join = tables_join[data['group_by']]
            join = f'LEFT JOIN {table_join} as b on a.{group_by} = b.id'
            group_by = 'b.name'

        shops = ''
        if data.get('shops'):
            field_shops = tuple(data['shops'])
            if len(field_shops) == 1:
                field_shops = str(field_shops).replace(',', '')
            shops = f" and a.shop in {field_shops}"

        agents = ''
        if data.get('agents'):
            field_agents = tuple(data['agents'])
            if len(field_agents) == 1:
                field_agents = str(field_agents).replace(',', '')
            agents = f" and a.agent in {field_agents}"

        payment_terms = ''
        if data.get('payment_terms'):
            field_payment_terms = tuple(data['payment_terms'])
            if len(field_payment_terms) == 1:
                field_payment_terms = str(field_payment_terms).replace(',', '')
            payment_terms = f" and a.payment_term in {field_payment_terms}"

        dates = f"and {column_date} >= '{data['start_date'].strftime('%Y-%m-%d')}' and {column_date} <= '{data['end_date'].strftime('%Y-%m-%d')}'"
        where = f"WHERE a.company = {data['company']} {state} {dates} {shops} {agents} {payment_terms}"""
        other_where = cls.get_other_where(data)
        where += other_where

        query = f"""
            SELECT
                COALESCE({group_by}, 'others'),
                EXTRACT(MONTH FROM {column_date})::integer AS month,
                {column_amount}
            FROM {table} as a
            {join}
            {where}
            GROUP BY {group_by}, EXTRACT(MONTH FROM {column_date});
        """
        cursor.execute(query)
        result = cursor.fetchall()
        months_dict = cls.get_months_dict()
        data_group = {}
        total_months = {**months_dict}
        total_months['total'] = 0
        for row in result:
            key = row[0]
            amount = row[2] or 0
            if key not in data_group:
                value = {
                    'group': key,
                    **months_dict,
                }
                value['total'] = 0
                data_group[key] = value
            data_group[key][row[1]] = amount
            data_group[key]['total'] += amount
            total_months[row[1]] += amount
            total_months['total'] += amount
        return data_group, total_months

    @classmethod
    def get_months_dict(cls):
        months = {}
        for m in range(1, 13):
            months[m] = 0
        return months

    @classmethod
    def get_other_where(cls, data):
        return ''


class SalePaymentForm(ModelView):
    "Sale Payment Form"
    __name__ = 'sale.payment.form'
    statement = fields.Many2One('account.statement', 'Statement',
       required=True)
    amount_payable = fields.Numeric('Amount Payable', required=True,
        digits=(16, Eval('currency_digits', 2)),
        depends=['currency_digits'])
    currency_digits = fields.Integer('Currency Digits')
    party = fields.Many2One('party.party', 'Party', readonly=True)
    require_voucher = fields.Boolean('Require Voucher',
        depends=['statement'])
    voucher = fields.Char('Voucher Number', states={
        'required': Eval('require_voucher', False),
        'invisible': Not(Eval('require_voucher', False)),
    }, depends=['require_voucher'])
    self_pick_up = fields.Boolean('Self Pick Up', readonly=True)
    do_invoice = fields.Boolean('Do Invoice')
    advance = fields.Boolean('Advance')
    advance_account = fields.Many2One('account.account', 'Advance Account')

    @classmethod
    def __setup__(cls):
        super(SalePaymentForm, cls).__setup__()

    @fields.depends('statement', 'voucher')
    def on_change_with_require_voucher(self):
        if self.statement:
            return self.statement.journal.require_voucher
        else:
            return False

    @classmethod
    def default_require_voucher(cls):
        return False

    @classmethod
    def default_do_invoice(cls):
        return True


class WizardSalePayment(Wizard):
    "Wizard Sale Payment"
    __name__ = 'sale.payment'
    start = StateView(
        'sale.payment.form',
        'sale_shop.sale_payment_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Pay', 'pay_', 'tryton-ok', default=True),
        ])
    pay_ = StateTransition()

    @classmethod
    def __setup__(cls):
        super(WizardSalePayment, cls).__setup__()

    @classmethod
    def execute(cls, session_id, data, state_name):
        pool = Pool()
        Sale = pool.get('sale.sale')
        sale = Sale(Transaction().context['active_id'])
        result = super().execute(session_id, data, state_name)
        dom_statement = [
            ('state', '=', 'draft'),
        ]
        context = Transaction().context

        if context.get('sale_device', None) and sale.sale_device:
            dom_statement.append(('sale_device', '=', sale.sale_device.id))

        if result.get('view'):
            result['view']['fields_view']['fields']['statement']['domain'] = dom_statement
        return result

    def default_start(self, fields):
        pool = Pool()
        Sale = pool.get('sale.sale')
        config = pool.get('sale.configuration')(1)
        sale = Sale(Transaction().context['active_id'])
        return {
            'amount_payable': abs(sale.total_amount - sale.paid_amount
                if sale.paid_amount else sale.total_amount),
            'currency_digits': sale.currency.digits,
            'party': sale.party.id,
            'do_invoice': config.do_invoice,
            'advance_account': config.advance_account.id if config.advance_account else None,
        }

    def transition_pay_(self):
        pool = Pool()
        Sale = pool.get('sale.sale')
        StatementLine = pool.get('account.statement.line')
        active_id = Transaction().context.get('active_id', False)
        sale = Sale(active_id)
        form = self.start
        if form.amount_payable > 0:
            if not sale.number:
                Sale.set_number([sale])

            if not sale.party.account_receivable:
                raise PartyMissingAccount(
                    gettext('sale_shop.msg_party_without_account_receivable', s=sale.party.name))
            config = pool.get('sale.configuration')(1)
            st_line = self.get_statement_line(sale, config)
            if st_line:
                line, = StatementLine.create([st_line])
                line.create_move()

        if sale.total_amount != sale.paid_amount:
            return 'start'
        return 'end'

    def get_statement_line(self, sale, config):
        pool = Pool()
        Date = pool.get('ir.date')
        form = self.start
        account = sale.party.account_receivable.id
        if form.advance and config.advance_account:
            account = config.advance_account
        amount = form.amount_payable

        if sale.total_amount < 0:
            amount = amount * -1

        st_line = {
            'statement': form.statement.id,
            'date': Date.today(),
            'amount': amount,
            'party': sale.party.id,
            'account': account,
            'description': self.start.voucher,
            'sale': sale.id,
            'number': sale.number,
        }
        return st_line


class MultiplePaymentSaleStart(ModelView):
    "Multiple Payment Sale Start"
    __name__ = 'multiple_payment_sale.start'

    statement = fields.Many2One('account.statement', 'Statement',
       required=True, domain=[('state', '=', 'draft')])
    party = fields.Many2One('party.party', 'Party', required=True)
    amount = fields.Numeric('Amount', digits=(16, 2))
    sales = fields.One2Many('select_multiple_payment_sale', 'line', 'Sales',
        context={'party': Eval('party')})
    balance = fields.Numeric('Balance', digits=(16, 2), readonly=True)
    id = fields.Integer('Id', readonly=True)
    company = fields.Many2One('company.company', 'Company', readonly=True)
    description = fields.Char('Description')

    @staticmethod
    def default_id():
        return 1

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_amount():
        return Decimal(0)

    @staticmethod
    def default_balance():
        return Decimal(0)

    @fields.depends('sales', 'amount')
    def on_change_sales(self):
        if self.amount:
            self.balance = self.amount - sum(s.amount_to_pay for s in self.sales if s.amount_to_pay)

    @fields.depends('sales', 'amount')
    def on_change_amount(self):
        if self.amount and self.sales:
            self.balance = self.amount - sum(s.amount_to_pay for s in self.sales if s.amount_to_pay)


class SelectMultiplePaymentSale(ModelView):
    "Select Multiple Payment Sale"
    __name__ = 'select_multiple_payment_sale'

    line = fields.Many2One('multiple_payment_sale.start', 'Line Id')
    sale = fields.Many2One('sale.sale', 'Sale', domain=[
        ('state', 'in', ['confirmed', 'processing']),
        ('party', '=', Eval('party')),
        ], depends=['party'])
    residual_amount = fields.Numeric('residual amount', digits=(16, 2),
        readonly=True)
    amount_to_pay = fields.Numeric('amount to pay', digits=(16, 2),
        help="Amount to pay must be less than balance")
    party = fields.Many2One('party.party', 'Party')
    company = fields.Many2One('company.company', 'Company', readonly=True)

    @staticmethod
    def default_line():
        return 1

    @fields.depends(
        'line', '_parent_line.party', '_parent_line.company')
    def on_change_line(self):
        if self.line:
            self.party = self.line.party
            self.company = self.line.company

    @fields.depends('residual_amount', 'amount_to_pay',
        'sale', 'line', '_parent_line.balance')
    def on_change_sale(self):
        if self.sale:
            residual = self.sale.residual_amount
            self.residual_amount = residual
            self.amount_to_pay = residual
        else:
            self.residual_amount = None
            self.amount_to_pay = None

    @fields.depends('amount_to_pay', 'line', 'residual_amount')
    def on_change_amount_to_pay(self):
        if self.amount_to_pay > self.line.balance or self.amount_to_pay > self.residual_amount:
            self.amount_to_pay = None


class MultiplePaymentSale(Wizard):
    "Multiple Payment Sale"
    __name__ = 'multiple_payment_sale'
    start = StateView(
        'multiple_payment_sale.start',
        'sale_shop.multiple_payment_sale_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Create Moves', 'create_moves', 'tryton-ok', default=True),
            Button('Select Sales', 'select_sales_ask'),
        ])
    select_sales_ask = StateView(
        'sale.shop.select_sales.ask',
        'sale_shop.select_sales_ask_view_form', [
            Button('Add', 'add_sales', 'tryton-ok', default=True),
        ])
    create_moves = StateTransition()
    add_sales = StateTransition()

    def default_select_sales_ask(self, fields):
        return {
            'party': self.start.party.id,
            'statement': self.start.statement.id,
        }

    def default_start(self, fields):
        default = {}
        if hasattr(self.select_sales_ask, 'party'):
            default.update({'party': self.select_sales_ask.party.id})
        if hasattr(self.select_sales_ask, 'statement'):
            default.update({'statement': self.select_sales_ask.statement.id})
        if hasattr(self.select_sales_ask, 'sales'):
            default.update(
                {'sales': [
                    {'sale': s.id, 'party': self.select_sales_ask.party.id} for s in self.select_sales_ask.sales
                ]})
        return default

    @classmethod
    def execute(cls, session_id, data, state_name):
        result = super().execute(session_id, data, state_name)
        if result.get('view') and state_name == 'select_sales_ask':
            sale_domain = [
                ('state', 'in', ['confirmed', 'processing']),
                ('invoice_state', '!=', 'paid'),
            ]
            if result['view']['defaults'].get('party'):
                sale_domain.append(('party', '=', result['view']['defaults']['party']))
            result['view']['fields_view']['fields']['sales']['domain'] = sale_domain
        return result

    def transition_create_moves(self):
        pool = Pool()
        StatementLine = pool.get('account.statement.line')
        statement = self.start.statement
        description = self.start.description
        party = self.start.party
        account = party.account_receivable
        for line in self.start.sales:
            st_line = StatementLine(
                    statement=statement,
                    amount=line.amount_to_pay,
                    party=party,
                    sale=line.sale,
                    account=account,
                    description=description,
                    date=date.today(),
                    number=line.sale.number,
                )
            st_line.save()
        return 'end'

    def transition_add_sales(self):
        return 'start'


class SelectSalesAsk(ModelView):
    "Select Sales Ask"
    __name__ = 'sale.shop.select_sales.ask'
    sales = fields.Many2Many('sale.sale', None, None, 'Sales')
    party = fields.Many2One('party.party', 'Party', readonly=True,
        states={'invisible': True})
    statement = fields.Many2One('account.statement', 'Statement',
        readonly=True, states={'invisible': True})
