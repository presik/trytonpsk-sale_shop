# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import Pool

from . import (
    configuration,
    invoice,
    price_list,
    product,
    sale,
    shop,
    source,
    statement,
    stock,
    user,
)


def register():
    Pool.register(
        configuration.Configuration,
        shop.SaleShop,
        shop.SaleShopResUser,
        # shop.SaleShopEmployee,
        shop.SaleShopExperiences,
        shop.SaleShopPromotions,
        user.User,
        sale.Sale,
        statement.Journal,
        statement.StatementLine,
        stock.ShipmentOut,
        stock.ShipmentOutReturn,
        sale.SaleBySupplierStart,
        sale.SaleLine,
        sale.SaleDetailedStart,
        sale.SalePaymentForm,
        invoice.Invoice,
        invoice.InvoicesStart,
        price_list.PriceListLine,
        product.UpdatePriceProductStart,
        product.Template,
        source.SaleSource,
        price_list.PriceListBySupplierStart,
        sale.SaleByMonthStart,
        sale.MultiplePaymentSaleStart,
        sale.SelectMultiplePaymentSale,
        sale.SelectSalesAsk,
        module='sale_shop', type_='model')
    Pool.register(
        sale.PrintSaleBySupplier,
        sale.SaleDetailed,
        invoice.Invoices,
        product.UpdatePriceProduct,
        price_list.PriceListBySupplier,
        sale.WizardSalePayment,
        sale.MultiplePaymentSale,
        sale.SaleByMonth,
        module='sale_shop', type_='wizard')
    Pool.register(
        sale.SaleBySupplier,
        sale.SaleDetailedReport,
        invoice.InvoicesReport,
        price_list.PriceListBySupplierReport,
        sale.SaleByMonthReport,
        module='sale_shop', type_='report')
