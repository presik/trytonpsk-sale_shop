import polars as pl

schema_query_sale = {
    'shop_name': pl.String,
    'date': pl.Date,
    'number': pl.String,
    'kind': pl.String,
    'state': pl.String,
    'agent': pl.Int64,
    'reference': pl.String,
    'party': pl.Int64,
    'comment': pl.String,
    'record_sale': pl.Int64,
    'create_date': pl.Datetime,
    'payment_term': pl.String,
    'payment_type': pl.String,
    'product': pl.Int64,
    'product_code': pl.String,
    'product_name': pl.String,
    'category_account': pl.String,
    'category_admin': pl.String,
    'default_uom': pl.Int64,
    'quantity': pl.Float32,
    'unit': pl.UInt16,
    'invoice_type': pl.String,
    'unit_price': pl.Float32,
    'base_price': pl.Float32,
    'description': pl.String,
    'note': pl.String,
    'uom': pl.String,
    'description_sale': pl.String,
    'cost_price': pl.Float32,
    'tax_rate': pl.Float32,
    'tax_amount': pl.Float32,
    'cost_price_sl': pl.Float32,
}

schema_query_agent = {
    'agent': pl.Int64,
    'agent_name': pl.String,
}

schema_query_party = {
    'record_sale': pl.Int64,
    'party_name': pl.String,
    'party_id_number': pl.String,
    'city': pl.String,
    'street': pl.String,
}

schema_query_consumer = {
    'record_sale': pl.Int64,
    'consumer_name': pl.String,
    'consumer_phone': pl.String,
}

schema_query_source = {
    'source': pl.Int64,
    'source_name': pl.String,
}

schema_query_discount = {
    'auth_discount': pl.Int64,
    'auth_discount_name': pl.String,
}

query_sale = """
    WITH product_dates AS (
        SELECT
            product,
            effective_date,
            cost_price
        FROM (
            SELECT DISTINCT ON (product, effective_date)
                product,
                effective_date,
                cost_price
            FROM (
                SELECT id, product, effective_date, cost_price, create_date
                FROM product_average_cost
                ORDER BY effective_date ASC, create_date DESC
            ) AS pac_ordered
        ) AS pac_distinct
    ),
    pac as (SELECT
        product,
        effective_date,
        cost_price,
        COALESCE(LEAD(effective_date) OVER (PARTITION BY product ORDER BY effective_date), CURRENT_DATE) AS end_date
    FROM product_dates),
    taxes_rate AS (
        SELECT sum(t.rate) AS rate, l.id
        FROM {table_line} as l
        LEFT JOIN {table} as i ON l.{column_table}=i.id
        LEFT JOIN {table_line}_account_tax AS lt ON lt.line=l.id
        LEFT JOIN account_tax  AS t ON t.id=lt.tax
        WHERE i.state in {states}
        AND i.{column_table}_date >= '{start_date}'
        AND i.{column_table}_date <= '{end_date}'
        AND t.classification_tax in ('01', '03', '04', '32', '34', '35')
        {where}
        GROUP BY l.id
    ),
    taxes_amount AS (
        SELECT p.extra_tax AS amount, l.id
        FROM {table_line} as l
        LEFT JOIN {table} as i ON l.{column_table}=i.id
        LEFT JOIN product_product as p ON p.id=l.product
        LEFT JOIN {table_line}_account_tax AS lt ON lt.line=l.id
        LEFT JOIN account_tax  AS t ON t.id=lt.tax
        WHERE i.state in {states}
        AND i.{column_table}_date >= '{start_date}'
        AND i.{column_table}_date <= '{end_date}'
        AND t.classification_tax in ('08')
        {where}
    ),
    admin_category AS (
        SELECT c.name AS category, cat.template
        FROM (
            SELECT DISTINCT ON (template)
            template, category
            FROM "product_template-product_category"
        ) AS cat
        LEFT JOIN product_category as c ON c.id=cat.category
    )
    SELECT sh.name as shop_name, i.{column_table}_date AS date,
    i.number, i.state, i.agent, i.reference,
    apt.name AS payment_term, i.id as record_sale,
    apt.payment_type, i.party, i.create_date,
    l.product, pt.code AS product_code, pt.name AS product_name,
    pcc.name AS category_account, i.comment,
    adc.category AS category_admin,
    pt.default_uom, l.quantity, l.unit,
    i.invoice_type, l.unit_price, i.description as description_sale,
    l.base_price, l.description,
    l.note, pac.cost_price, uom_s.symbol as uom,
    COALESCE(tr.rate, 0) AS tax_rate,
    COALESCE(ta.amount, 0) AS tax_amount,
    CASE
        WHEN (uom_d.factor=uom_s.factor) THEN
            COALESCE(pac.cost_price, 0)
        WHEN (uom_d.factor<uom_s.factor) THEN
            ROUND(COALESCE(pac.cost_price/uom_s.factor::numeric, 0), 2)
        ELSE
            ROUND(COALESCE(pac.cost_price*uom_s.rate::numeric, 0), 2)
    END AS cost_price_sl
    {other_columns}
    FROM {table_line} AS l
    INNER JOIN {table} AS i ON l.{column_table}=i.id
    INNER JOIN product_product AS p on l.product=p.id
    INNER JOIN product_template AS pt on p.template=pt.id
    INNER JOIN product_uom AS uom_d ON pt.default_uom=uom_d.id
    INNER JOIN product_uom AS uom_s ON l.unit=uom_s.id
    INNER JOIN account_invoice_payment_term AS apt on i.payment_term=apt.id
    LEFT JOIN product_category AS pcc on pt.account_category=pcc.id
    LEFT JOIN admin_category AS adc on pt.id=adc.template
    LEFT JOIN taxes_rate AS tr ON l.id=tr.id
    LEFT JOIN taxes_amount AS ta ON l.id=ta.id
    LEFT JOIN sale_shop AS sh ON i.shop=sh.id
    LEFT JOIN pac ON l.product=pac.product
    AND i.{column_table}_date>=pac.effective_date
    AND i.{column_table}_date<pac.end_date
    WHERE i.state in {states}
    AND i.{column_table}_date >= '{start_date}'
    AND i.{column_table}_date <= '{end_date}'
    {where};
"""


query_agent = """
    SELECT a.id as agent, p.name as agent_name
    FROM commission_agent AS a
    LEFT JOIN party_party as p ON a.party=p.id
"""

query_party = """
    SELECT i.id as record_sale, p.name as party_name, p.id_number as party_id_number,
    pad.city, pad.street
    FROM party_party AS p
    LEFT JOIN {table} AS i ON p.id=i.party
    LEFT JOIN party_address AS pad ON i.invoice_address=pad.id
    WHERE i.state in {states}
    AND i.{column_table}_date >= '{start_date}'
    AND i.{column_table}_date <= '{end_date}'
    {where};
"""

query_consumer = """
    SELECT i.id as record_sale, pc.name as consumer_name, pc.phone as consumer_phone
    FROM party_consumer AS pc
    LEFT JOIN sale_sale as i ON pc.id=i.consumer
    WHERE i.state in {states}
    AND i.{column_table}_date >= '{start_date}'
    AND i.{column_table}_date <= '{end_date}'
    {where};
"""

query_sale_source = """
    SELECT a.id as source, a.name as source_name
    FROM sale_source AS a
"""

query_sale_discount = """
    SELECT sd.id as auth_discount, sd.name as auth_discount_name
    FROM sale_discount AS sd
"""
