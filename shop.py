# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond import backend
from trytond.model import ModelSQL, ModelView, fields
from trytond.pool import Pool
from trytond.pyson import Bool, Eval, Id
from trytond.transaction import Transaction


class SaleShop(ModelSQL, ModelView):
    "Sale Shop"
    __name__ = 'sale.shop'
    name = fields.Char('Shop Name', required=True)
    users = fields.Many2Many('sale.shop-res.user', 'shop', 'user', 'Users')
    address = fields.Many2One('party.address', 'Address', domain=[
            ('party', '=', Eval('company_party')),
            ], depends=['company_party'])
    warehouse = fields.Many2One('stock.location', "Warehouse", required=True,
        domain=[('type', '=', 'warehouse')])
    currency = fields.Many2One('currency.currency', 'Currency', required=True)
    price_list = fields.Many2One('product.price_list', 'Pricelist', ondelete="RESTRICT")
    payment_term = fields.Many2One('account.invoice.payment_term',
        'Payment Term', required=True)
    sale_sequence = fields.Many2One('ir.sequence',
        'Sale Reference Sequence', domain=[
            ('company', 'in', [Eval('context', {}).get('company', 0),
                    None]),
            ('sequence_type', '=', Id('sale', 'sequence_type_sale')),
        ], required=True)
    sale_invoice_method = fields.Selection([
            ('manual', 'Manual'),
            ('order', 'On Order Processed'),
            ('shipment', 'On Shipment Sent'),
            ], 'Sale Invoice Method', states={
            'required': Bool(Eval('context', {}).get('company', 0)),
        })
    sale_shipment_method = fields.Selection([
            ('manual', 'Manual'),
            ('order', 'On Order Processed'),
            ('invoice', 'On Invoice Paid'),
            ], 'Sale Shipment Method', states={
            'required': Bool(Eval('context', {}).get('company', 0)),
        })
    company = fields.Many2One('company.company', 'Company', required=True,
        domain=[])
    company_party = fields.Function(fields.Many2One('party.party',
            'Company Party'), 'on_change_with_company_party')
    active = fields.Boolean('Active')
    self_pick_up = fields.Boolean('Self Pickup')
    sale_return_sequence = fields.Many2One('ir.sequence',
        'Sale Return Sequence', domain=[
            ('company', 'in',
                [Eval('context', {}).get('company', -1), None]),
            ('sequence_type', '=', Id('sale', 'sequence_type_sale')),
        ], required=True)
    # salesmans = fields.Many2Many('sale.shop.company_employee', 'shop',
    #                              'salesman', 'Salesmans')
    workflow_invoice = fields.Selection([
        ('', ''),
        ('validated', 'Validated'),
        ('posted', 'Posted'),
        ('reconciled', 'Reconciled'),
    ], 'Workflow Invoice')
    dataphone = fields.Char('Dataphone', help='set code Dataphone')
    tax_exempt = fields.Boolean('Tax Exempt')
    kind = fields.Selection([
            ('restaurant', 'Restaurant'),
            ('retail', 'Retail'),
            ('frontdesk', 'Frontdesk'),
            ('tickets', 'Tickets'),
            (None, ''),
            ], 'Kind')
    experiences = fields.Many2Many('sale.shop.experiences', 'shop', 'product', 'Experiences')
    promotions = fields.Many2Many('sale.shop.promotions', 'shop', 'product', 'Promotions')

    @classmethod
    def __register__(cls, module_name):
        pool = Pool()
        Company = pool.get('company.company')
        shop_table = cls.__table__()
        table_h = cls.__table_handler__(module_name)
        company_table = Company.__table__()
        # migrate column pos_terminal to dataphone in version 6.0.18
        if table_h.column_exist('pos_terminal'):
            table_h.column_rename('pos_terminal', 'dataphone')

        super(SaleShop, cls).__register__(module_name)
        cursor = Transaction().connection.cursor()
        if backend.name != 'sqlite':
            # SQLite doesn't support this query as it generates and update
            # with an alias (AS) which is not valid on SQLite
            query = shop_table.update(columns=[shop_table.currency],
                values=[company_table.currency],
                from_=[company_table],
                where=((shop_table.company == company_table.id)
                    & (shop_table.currency == None)))
            cursor.execute(*query)

    @staticmethod
    def default_active():
        return True

    @staticmethod
    def sale_configuration():
        Config = Pool().get('sale.configuration')
        config = Config(1)
        return config

    @fields.depends('company')
    def on_change_with_company_party(self, name=None):
        Company = Pool().get('company.company')
        company = self.company
        if not company:
            company = Company(SaleShop.default_company())
        return (company and company.party.id) or None


class SaleShopResUser(ModelSQL):
    "Sale Shop - Res User"
    __name__ = 'sale.shop-res.user'
    _table = 'sale_shop_res_user'
    shop = fields.Many2One('sale.shop', 'Shop', ondelete='CASCADE',
        required=True)
    user = fields.Many2One('res.user', 'User', ondelete='RESTRICT',
        required=True)


# class SaleShopEmployee(ModelSQL):
#     "Sale Shop - Employees"
#     __name__ = 'sale.shop.company_employee'
#     _table = 'sale_shop_company_employee'
#     shop = fields.Many2One('sale.shop', 'Shop', ondelete='CASCADE',
#         required=True)
#     salesman = fields.Many2One('company.employee', 'Salesman', states={
#         'readonly': Eval('state').in_(['confirmed', 'processing', 'done']),
#     }, ondelete='RESTRICT', required=True)


class SaleShopExperiences(ModelSQL):
    "Sale Shop - Experiences"
    __name__ = 'sale.shop.experiences'
    _table = 'sale_shop_experiences'
    shop = fields.Many2One('sale.shop', 'Shop', ondelete='CASCADE',
        required=True)
    product = fields.Many2One('product.product', 'Product', required=True, ondelete='CASCADE')


class SaleShopPromotions(ModelSQL):
    "Sale Shop - Promotions"
    __name__ = 'sale.shop.promotions'
    _table = 'sale_shop_promotions'
    shop = fields.Many2One('sale.shop', 'Shop', ondelete='CASCADE',
        required=True)
    product = fields.Many2One('product.product', 'Product', required=True, ondelete='CASCADE')
